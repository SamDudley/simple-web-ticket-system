A very simple web ticket system I put together.

* Built using Flask.
* Uses ajax to request the web ticket template.
* Web tickets are stored in an sqlite database.
* Currently can add, view, and delete tickets.