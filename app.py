from flask import Flask
from flask import render_template
from flask import request
from flask_sqlalchemy import SQLAlchemy
import datetime as dt


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///tickets.db'
db = SQLAlchemy(app)


class Ticket(db.Model):
    # id should be a unique auto-incrementing value.
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.String)
    name = db.Column(db.String)
    email = db.Column(db.String)
    subject = db.Column(db.String)
    details = db.Column(db.String)

    def __init__(self, date, name, email, subject, details):
        self.date = date
        self.name = name
        self.email = email
        self.subject = subject
        self.details = details


# This route is only for testing whilst developing.
@app.route('/test')
def test():
    return render_template('test.html')


@app.route('/webticketform')
def web_ticket_form():
    return render_template('web_ticket_form.html')


@app.route('/submit', methods=['POST'])
def submit():
    # For the moment the date is taken from the server.
    date = dt.datetime.now()
    name = request.form['name']
    email = request.form['email']
    subject = request.form['subject']
    details = request.form['details']
    ticket = Ticket(date, name, email, subject, details)
    db.session.add(ticket)
    db.session.commit()
    return render_template('test.html')


@app.route('/view-tickets')
def view_tickets():
    tickets = Ticket.query.all()
    return render_template('view_tickets.html',
                           tickets=tickets)


@app.route('/ticket/<id>')
def one_ticket(id):
    ticket = Ticket.query.get(id)
    return render_template('ticket.html',
                           ticket=ticket)


@app.route('/ticket/<id>/delete')
def delete_ticket(id):
    ticket = Ticket.query.get(id)
    db.session.delete(ticket)
    db.session.commit()
    return render_template('test.html')


if __name__ == '__main__':
    app.run(debug=True)
