function getFormTemplate(callback) {
    var xmlhttp;
    xmlhttp = new XMLHttpRequest;
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            callback(xmlhttp.responseText);
        }
    }
    xmlhttp.open('GET', '/webticketform');
    xmlhttp.send();
}

function setupForm(template) {
    var webTicket = document.getElementById('web-ticket-form');
    webTicket.innerHTML = template;
}

getFormTemplate(setupForm);
